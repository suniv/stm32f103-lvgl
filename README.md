![STM32F103 LVGl DEMO](./docs/stm32f103lvgl.png)

项目效果演示视频：[https://www.bilibili.com/video/BV1im4y1X7g4](https://www.bilibili.com/video/BV1im4y1X7g4)

# 说明

[韦东山百问网](https://www.100ask.net)LVGL单片机示例程序，本项目适用于[STM32F103开发板](https://detail.tmall.com/item.htm?id=638105716849)。

| 名称 | 说明 |
| ---- | ---- |
| Core | 从SDK抽取的核心文件，方便修改 | 
| Driver | 用户编写的外设驱动文件 |
| Main | 主函数文件 | 
| Project | 工程文件，包含目标二进制文件 |
| SDK | CMSIS和HAL库，只包含用到的库，添加其它外设需要添加相应库 | 
| Clean.bat | 清理工程产生的文件，缩小体积，方便拷贝 | 
| Readme.txt | 工程说明文件 |

# 韦东山百问网LVGL教程
## 视频教程

视频教程已发布！获取更新动态，请关注(任意一个)：

- [https://www.bilibili.com/video/BV1Ya411r7K2](https://www.bilibili.com/video/BV1Ya411r7K2)
- [https://www.100ask.net/detail/p_61c5a317e4b0cca4a4e8b6f1/6](https://www.100ask.net/detail/p_61c5a317e4b0cca4a4e8b6f1/6)

## 文档教程
- [http://lvgl.100ask.net/](http://lvgl.100ask.net/)

# 开发板购买地址

- [STM32F103开发板](https://detail.tmall.com/item.htm?id=638105716849)：[https://detail.tmall.com/item.htm?id=638105716849](https://detail.tmall.com/item.htm?id=638105716849)



