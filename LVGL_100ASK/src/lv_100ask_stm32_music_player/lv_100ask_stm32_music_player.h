#ifndef LV_100ASK_STM32_MUSIC_PLAYER_H
#define LV_100ASK_STM32_MUSIC_PLAYER_H

#ifdef __cplusplus
extern "C" {
#endif


/*********************
 *      INCLUDES
 *********************/
#include "../../lv_100ask.h"

/*********************
 *      DEFINES
 *********************/
#define LV_100ASK_MUSIC_PLAYER_SPACE                 (75)
#define LV_100ASK_MUSIC_PLAYER_BUFSIZE				 (1504) //1408 //(1312)	//(1248) //1024*2
#define LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET          (LV_VER_RES / 3 + 20)
#define LV_100ASK_MUSIC_PLAYER_LIST_Y_OFFSET		 (LV_VER_RES / 3)
#define LV_100ASK_MUSIC_PLAYER_LIST_HIGHT		 	 (LV_VER_RES - (LV_VER_RES / 3))
#define LV_100ASK_MUSIC_PLAYER_LIST_ANIM_Y		 	 (300)
#define LV_100ASK_MUSIC_PLAYER_BTN_SPACE             ((LV_HOR_RES - LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE - (LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE * 4)) / 7)
#define LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE         (50)
#define LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE        (40)
#define LV_100ASK_MUSIC_PLAYER_SLIDER_SIZE           ((LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE) + (LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE * 4) + (LV_100ASK_MUSIC_PLAYER_BTN_SPACE * 4))
#define LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH (128)
#define LV_100ASK_MUSIC_PLAYER_DEFAULT_MUSIC 		 ("S:/music/TestFile.mp3")
#define LV_100ASK_MUSIC_PLAYER_DEFAULT_PATH 		 ("S:/music/")



/**********************
 *      TYPEDEFS
 **********************/
typedef struct _lv_100ask_music_player {
	lv_obj_t  * bg_music_player;      		/* 背景 */
	lv_obj_t  * current_btn;				/* 记录当前播放的索引 */
	lv_obj_t  * slider_label;				/* 播放进度条 */
	lv_obj_t  * label_music_name;			/* 播放的文件名 */
	lv_obj_t  * music_list;    	   			/* 播放列表句柄 */
	lv_obj_t  * btn_player_ctl; 			/* 暂停、播放按钮 */
	lv_obj_t  * btn_per; 					/* 上一首按钮 */
	lv_obj_t  * btn_next;     				/* 下一首按钮 */
	lv_obj_t  * btn_shuffle; 				/* 随机播放按钮 */
	lv_obj_t  * btn_list;					/* 播放列表按钮 */
	lv_obj_t  * slider;						/* 播放进度条 */
	lv_task_t * task_handle;   				/* 播放任务句柄 */
	
	lv_fs_file_t file;						/* 文件对象 */
	uint8_t  buffer[LV_100ASK_MUSIC_PLAYER_BUFSIZE];				/* 缓冲区 */
	uint32_t progress;  					/* 播放进度 */
	bool player_state;   					/* 播放状态： 1 - 播放中 ; 0 - 暂停播放 */
} T_lv_100ask_music_player, *PT_lv_100ask_music_player;


/**********************
 * GLOBAL PROTOTYPES
 **********************/
void lv_100ask_stm32_music_player(void);


/**********************
 *      MACROS
 **********************/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LV_100ASK_STM32_MUSIC_PLAYER_H */

