#ifndef LV_100ASK_STM32_TOOL_HUMITURE_H
#define LV_100ASK_STM32_TOOL_HUMITURE_H

#ifdef __cplusplus
extern "C" {
#endif


/*********************
 *      INCLUDES
 *********************/
#include "../../lv_100ask.h"


/**********************
 *  	EXTERN
 **********************/
extern void us_timer_delay(uint16_t t);


/*********************
 *      DEFINES
 *********************/
// Layout
#define LV_100ASK_HUMITURE_SPACE         (80)
#define LV_100ASK_HUMITURE_POINT_COUNT   (20)

// TIME
#define LV_100ASK_HUMITURE_UPDATE_TIME   (2000)

// DHT11
#define LV_100ASK_DHT11_DELAY(N)      us_timer_delay(N*10)
#define LV_100ASK_DHT11_PIN           GPIO_PIN_10
#define LV_100ASK_DHT11_PORT          GPIOB
#define LV_100ASK_DHT11_CLK_EN()      __HAL_RCC_GPIOB_CLK_ENABLE()
#define LV_100ASK_DHT11_CLK_DIS()     __HAL_RCC_GPIOB_CLK_DISABLE()
#define LV_100ASK_DHT11_O(VALUE)      HAL_GPIO_WritePin(LV_100ASK_DHT11_PORT, LV_100ASK_DHT11_PIN, VALUE)
#define LV_100ASK_DHT11_I             HAL_GPIO_ReadPin(LV_100ASK_DHT11_PORT, LV_100ASK_DHT11_PIN)


/**********************
 *      TYPEDEFS
 **********************/
typedef struct _lv_100ask_humiture {
	lv_obj_t * bg_humiture;          		// 背景
	lv_obj_t * chart_temperature;  			// 温度
	lv_obj_t * chart_humidity;     			// 湿度
	lv_chart_series_t * ser_temperature;	// 绘制温度图表
	lv_chart_series_t * ser_humidity;		// 绘制湿度图表
	uint16_t mTempture;						// 温度数据
	uint16_t mHumidity;						// 湿度数据
} T_lv_100ask_humiture, *PT_lv_100ask_humiture;


/**********************
 * GLOBAL PROTOTYPES
 **********************/
void lv_100ask_stm32_tool_humiture(void);


/**********************
 *      MACROS
 **********************/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LV_100ASK_STM32_TOOL_HUMITURE_H */





