#ifndef LV_100ASK_STM32_TOOL_ADC_H
#define LV_100ASK_STM32_TOOL_ADC_H

#ifdef __cplusplus
extern "C" {
#endif


/*********************
 *      INCLUDES
 *********************/
#include "../../lv_100ask.h"


/*********************
 *      DEFINES
 *********************/
#define LV_100ASK_WIDGET_TEST_SPACE         (50)


/**********************
 *      TYPEDEFS
 **********************/
typedef struct _lv_100ask_widget_test {
	lv_obj_t * bg_widget_test;     		// ����
	lv_obj_t * adc_gauge;	 			// չʾadcʵʱ���ֵ
	lv_task_t * task_handle;			// ������(������)
} T_lv_100ask_widget_test, *PT_lv_100ask_widget_test;


/**********************
 * GLOBAL PROTOTYPES
 **********************/
void lv_100ask_stm32_tool_widget_test(void);

/**********************
 *      MACROS
 **********************/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LV_100ASK_STM32_TOOL_ADC_H */





