/**
 ******************************************************************************
 * @file    lv_100ask_stm32_tool_widget_test.c
 * @author  百问科技
 * @version V1.2
 * @date    2020-12-12
 * @brief	板载功能测试(ADC、LED、蜂鸣器)
 ******************************************************************************
 * Change Logs:
 * Date           Author          Notes
 * 2020-12-12     zhouyuebiao     First version
 * 2021-01-25     zhouyuebiao     V1.2 
 ******************************************************************************
 * @attention
 *
 * Copyright (C) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 * All rights reserved
 *
 ******************************************************************************
 */
 
/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include <stdlib.h>
#include "lv_100ask_stm32_tool_widget_test.h"
#include "ADC_SoftwareTriger.h"
#include "driver_led.h"
#include "driver_Buzzer.h"


/**********************
 *  STATIC VARIABLES
 **********************/
static PT_lv_100ask_widget_test g_pt_lv_100ask_widget_test;  // 结构体


/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_100ask_stm32_tool_widget_test_init(void);
static void lv_100ask_stm32_tool_led_beep_init(lv_obj_t * offset_obj);
static void lv_task_100ask_stm32_buzzer(lv_task_t * task);									// 蜂鸣器鸣叫任务
static void lv_100ask_stm32_tool_test_event_handler(lv_obj_t * obj, lv_event_t event);
static void lv_100ask_stm32_tool_adc_gauge_anim(lv_obj_t * gauge, lv_anim_value_t value);
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event);

	
/*
 *  函数名：  void lv_100ask_stm32_tool_widget_test(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：应用初始化入口
*/
void lv_100ask_stm32_tool_widget_test(void)
{
	/* 申请内存 */
	g_pt_lv_100ask_widget_test = (T_lv_100ask_widget_test *)malloc(sizeof(T_lv_100ask_widget_test));

	/* 创建背景 */
	g_pt_lv_100ask_widget_test->bg_widget_test = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(g_pt_lv_100ask_widget_test->bg_widget_test, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_y(g_pt_lv_100ask_widget_test->bg_widget_test, 0);

	/* 主界面初始化 */
    lv_100ask_stm32_tool_widget_test_init();
	
	/* 创建蜂鸣器任务 */
	g_pt_lv_100ask_widget_test->task_handle = lv_task_create(lv_task_100ask_stm32_buzzer, 5, LV_TASK_PRIO_MID, NULL);	
	lv_task_set_prio(g_pt_lv_100ask_widget_test->task_handle, LV_TASK_PRIO_OFF);  		// 等待用户点击鸣叫

	/* 创建app标题 */
    add_title(g_pt_lv_100ask_widget_test->bg_widget_test, "TEST");					

	/* 创建返回桌面按钮 */
	add_back(g_pt_lv_100ask_widget_test->bg_widget_test, event_handler_back_to_home);
}


/*
 *  函数名：  static void lv_100ask_stm32_tool_widget_test_init(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：主界面初始化
*/
static void lv_100ask_stm32_tool_widget_test_init(void)
{
    lv_anim_t a;      // 表盘动画
    lv_anim_init(&a);

    g_pt_lv_100ask_widget_test->adc_gauge = lv_gauge_create(g_pt_lv_100ask_widget_test->bg_widget_test, NULL);
    lv_gauge_set_range(g_pt_lv_100ask_widget_test->adc_gauge, 0 ,3000);
	lv_gauge_set_critical_value(g_pt_lv_100ask_widget_test->adc_gauge, 2400);  // 设置临界值，超过临界值部分的颜色为 scale_end_color
    lv_obj_set_drag_parent(g_pt_lv_100ask_widget_test->adc_gauge, true);
    lv_obj_set_size(g_pt_lv_100ask_widget_test->adc_gauge, 300, 300);
    lv_obj_align(g_pt_lv_100ask_widget_test->adc_gauge, NULL, LV_ALIGN_IN_TOP_MID, 0, LV_100ASK_WIDGET_TEST_SPACE);
    lv_obj_set_style_local_margin_top(g_pt_lv_100ask_widget_test->adc_gauge, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, LV_DPX(30));
    lv_obj_set_style_local_value_font(g_pt_lv_100ask_widget_test->adc_gauge, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_32);	// 设置文字大小
    lv_obj_set_style_local_value_str(g_pt_lv_100ask_widget_test->adc_gauge, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, "ADC");
    lv_obj_set_style_local_value_align(g_pt_lv_100ask_widget_test->adc_gauge, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, LV_ALIGN_CENTER);
    lv_obj_set_style_local_value_ofs_y(g_pt_lv_100ask_widget_test->adc_gauge, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, LV_DPX(80));

    lv_obj_t * label = lv_label_create(g_pt_lv_100ask_widget_test->adc_gauge, NULL);
    lv_obj_set_style_local_text_font(label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_14);             // 设置文字大小
    lv_obj_align(label, g_pt_lv_100ask_widget_test->adc_gauge, LV_ALIGN_CENTER, 0, lv_obj_get_width(g_pt_lv_100ask_widget_test->adc_gauge)/4);

    lv_anim_set_var(&a, g_pt_lv_100ask_widget_test->adc_gauge);
    lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_adc_gauge_anim);
    lv_anim_set_values(&a, 0, 1);
    lv_anim_set_time(&a, 100);
    lv_anim_set_repeat_count(&a, LV_ANIM_REPEAT_INFINITE);
    lv_anim_start(&a);
	
	lv_100ask_stm32_tool_led_beep_init(g_pt_lv_100ask_widget_test->adc_gauge);
}



/*
 *  函数名：  static void lv_100ask_stm32_tool_led_beep_init(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：部件初始化
*/
static void lv_100ask_stm32_tool_led_beep_init(lv_obj_t * offset_obj)
{
	lv_obj_t * label;
    lv_obj_t * btn;
    btn = lv_btn_create(g_pt_lv_100ask_widget_test->bg_widget_test, NULL);
    lv_obj_set_size(btn, 100, 50);
    lv_obj_set_style_local_radius(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 0);          	// 设置圆角
    lv_obj_set_style_local_value_str(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "RED0");  	// 设置字符及led灯状态
    lv_obj_set_style_local_value_opa(btn, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0);       	// 设置字符透明度
    lv_obj_align(btn, offset_obj, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 10);
    lv_obj_set_event_cb(btn, lv_100ask_stm32_tool_test_event_handler);
    label = lv_label_create(btn, NULL);
    lv_label_set_text(label, "RED");

    btn = lv_btn_create(g_pt_lv_100ask_widget_test->bg_widget_test, NULL);
    lv_obj_set_size(btn, 100, 50);
    lv_obj_set_style_local_radius(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 0);          	// 设置圆角
    lv_obj_set_style_local_value_str(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "GREEN0");  	// 设置字符及led灯状态
    lv_obj_set_style_local_value_opa(btn, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0);       	// 设置字符透明度
    lv_obj_align(btn, offset_obj, LV_ALIGN_OUT_BOTTOM_LEFT, 100, 10);
    lv_obj_set_event_cb(btn, lv_100ask_stm32_tool_test_event_handler);
    label = lv_label_create(btn, NULL);
    lv_label_set_text(label, "GREEN");

    btn = lv_btn_create(g_pt_lv_100ask_widget_test->bg_widget_test, NULL);
    lv_obj_set_size(btn, 100, 50);
    lv_obj_set_style_local_radius(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 0);          	// 设置圆角
    lv_obj_set_style_local_value_str(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "BLUE0");  	// 设置字符及led灯状态
    lv_obj_set_style_local_value_opa(btn, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0);       	// 设置字符透明度
    lv_obj_align(btn, offset_obj, LV_ALIGN_OUT_BOTTOM_LEFT, 100*2, 10);
    lv_obj_set_event_cb(btn, lv_100ask_stm32_tool_test_event_handler);
    label = lv_label_create(btn, NULL);
    lv_label_set_text(label, "BLUE");


    btn = lv_btn_create(g_pt_lv_100ask_widget_test->bg_widget_test, NULL);
    lv_obj_set_size(btn, 300, 50);
    lv_obj_set_style_local_radius(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 0);          	// 设置圆角
    lv_obj_set_style_local_value_str(btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "BEEP0");  	// 设置字符及led灯状态
    lv_obj_set_style_local_value_opa(btn, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0);       	// 设置字符透明度
    lv_obj_align(btn, offset_obj, LV_ALIGN_OUT_BOTTOM_MID, 0, (70));
    lv_obj_set_event_cb(btn, lv_100ask_stm32_tool_test_event_handler);
    label = lv_label_create(btn, NULL);
    lv_label_set_text(label, "BEEP");
}



/*
 *  函数名：   static void lv_100ask_stm32_tool_adc_gauge_anim(lv_obj_t * gauge, lv_anim_value_t value)
 *  输入参数： 动画对象
 *  输入参数： 传递给动画的数据
 *  返回值：   无
 *  函数作用： 更新展示ADC检测值
*/
static void lv_100ask_stm32_tool_adc_gauge_anim(lv_obj_t * gauge, lv_anim_value_t value)
{
    float f_value;
    uint16_t adc_value = 0.0;
    
    f_value = GetAdcAnlogValue();
    adc_value = f_value*1000;

    lv_gauge_set_value(gauge, 0, adc_value);

    char tmpchar[32] = {0};

    sprintf(tmpchar, "AD:%d\nVoltage:%fV", adc_value, (float)adc_value*(3.3/4096));

    lv_obj_t * label = lv_obj_get_child(gauge, NULL);
    lv_label_set_text(label, tmpchar);
    lv_obj_align(label, gauge, LV_ALIGN_IN_TOP_MID, 0, lv_obj_get_y(label));
}



/*
 *  函数名：  static void lv_task_100ask_stm32_buzzer(lv_task_t * task)
 *  输入参数：任务描述符
 *  返回值：  无
 *  函数作用：蜂鸣器鸣叫任务
*/
static void lv_task_100ask_stm32_buzzer(lv_task_t * task)
{
	static bool buzzer_state = true;
	if (buzzer_state)
	{
		Buzzer(1);
		LEDRED_TOGGLE;
		LEDGREEN_TOGGLE;
		LEDBLUE_TOGGLE;
		buzzer_state = false;
	}
	else
	{
		Buzzer(0);
		buzzer_state = true;
	}
}


/*
 *  函数名：  static void lv_100ask_stm32_tool_test_event_handler(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：响应LED、蜂鸣器点击处理事件
*/
static void lv_100ask_stm32_tool_test_event_handler(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "RED0") == 0)
        {
			LEDRED_ON;
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "RED1");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_RED);
            return;
        }
        else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "RED1") == 0)
        {
			LEDRED_OFF;
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "RED0");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
            return;
        }

        if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "GREEN0") == 0)
        {
			LEDGREEN_ON;
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "GREEN1");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
            return;
        }
        else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "GREEN1") == 0)
        {
			LEDGREEN_OFF;
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "GREEN0");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
            return;
        }

        if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "BLUE0") == 0)
        {
			LEDBLUE_ON;
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "BLUE1");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLUE);
            return;
        }
        else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "BLUE1") == 0)
        {
			LEDBLUE_OFF;
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "BLUE0");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
            return;
        }
		
		if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "BEEP0") == 0)
        {
			lv_task_set_prio(g_pt_lv_100ask_widget_test->task_handle, LV_TASK_PRIO_MID);  		 		// 继续鸣叫任务
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "BEEP1");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_ORANGE);
            return;
        }
        else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), "BEEP1") == 0)
        {
			lv_task_set_prio(g_pt_lv_100ask_widget_test->task_handle, LV_TASK_PRIO_OFF);  		 		// 暂停鸣叫任务
            lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "BEEP0");  		// 设置字符及led灯状态
            lv_obj_set_style_local_bg_color(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
            return;
        }
    }
}


/*
 *  函数名：  static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：返回桌面事件处理函数
*/
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		/* 删除蜂鸣器鸣叫任务 */
		if (g_pt_lv_100ask_widget_test->task_handle != NULL)
			lv_task_del(g_pt_lv_100ask_widget_test->task_handle);  		
		
		/* 删除动画 */
		if (g_pt_lv_100ask_widget_test->adc_gauge != NULL)
		{
			lv_anim_del(g_pt_lv_100ask_widget_test->adc_gauge, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_adc_gauge_anim); 
			lv_obj_del(g_pt_lv_100ask_widget_test->adc_gauge);
		}
		
		/* 熄灭LED灯 */
		LEDRED_OFF;
		LEDGREEN_OFF;
		LEDBLUE_OFF;
				
		/* 删除背景 */
        if (g_pt_lv_100ask_widget_test->bg_widget_test != NULL)	
			lv_obj_del(g_pt_lv_100ask_widget_test->bg_widget_test);

		/* 释放内存 */
		free(g_pt_lv_100ask_widget_test);
		
		/* 清空屏幕并返回桌面 */
        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
        lv_100ask_stm32_demo_home(10);
    }
}
